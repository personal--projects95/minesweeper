const cellValues = new Array();
const boardGame = new Array();
const bombs = new Array();
let maxRows = 15, maxColumns = 15;
let maxBombsNo = 15;
let flagNo = maxBombsNo;
let cellsMarked = 0;
let isBomb = false;

createGameboard();
flagsRemaining();
placeBombs();

function createGameboard(){
  for (let row = 1; row <= maxRows; ++row) {
    boardGame[row] = [];
    let  rowNo = document.createElement("tr");
    rowNo.id = "row["+row+"]";
    document.getElementById("grid").appendChild(rowNo); 
    for (let column = 1; column <= maxColumns; ++column) {
      boardGame[row][column] = (row * maxRows) + column; 
      let cell = document.createElement("td");
      cell.classList.add("cell");
      cell.id = boardGame[row][column];       
      cell.addEventListener("mousedown", event => {
        if (event.button === 0) {         
          checkCell(row, column);
        } else if (event.button === 2) {
          flagCell(row, column); 
        }
      }); 
      document.getElementById("row["+row+"]").appendChild(cell);
    }
  }
}  

function checkCell(row, column) {
  if (bombs[row][column] === 1) {
    isBomb = true;
    document.getElementById(boardGame[row][column]).style.backgroundColor = "red";
    revealsAllBombs();
  } else {
    checkAdj(row, column);
  }
  checkIfWon();
} 

function checkAdj(row, column) {
  if (isOutsideBoard(row, column)) {
    return;
  } else if (isRevealed(row, column)) {
    return;
  } else {
    revealCell(row, column);
    if (hasAdjBombs(row, column)) {
      return;
    } else {
      revealAdjCells(row, column);
    }
  }
}

function revealAdjCells(row, column) {
  for (let a = row - 1; a <= row + 1; ++a) {
    for (let b = column - 1; b <= column + 1; ++b) {
      checkAdj(a, b);
    }
  }
}

function checkIfWon() {
  if (isBomb === true) {
    document.getElementById("status").innerHTML = "Game over";
  } else if ((isBomb === false) && (cellsMarked === maxRows * maxColumns)) {
    document.getElementById("status").innerHTML = "You won!";
  }
}

function flagsRemaining() {
  document.getElementById("flags").innerHTML = "Flags remained: " + flagNo;
}

function isOutsideBoard (row, column) {
  if ((row < 1) || (row > maxRows) || (column < 1) || (column > maxColumns)) {
    return true;
  } 
  return false;
}

function revealsAllBombs() {
  for (let row = 1; row <= maxRows; ++row) {
    for (let column = 1; column <= maxColumns; ++column){
    if (bombs[row][column] === 1) {
      document.getElementById(boardGame[row][column]).style.backgroundColor = "red";
      document.getElementById(boardGame[row][column]).innerHTML = "";
    }
    }
  }
}

function hasAdjBombs(row, column) {
  if (cellValues[row][column] != 0){
    return true;
  } 
  return false;
}

function revealCell(row, column) {
  if (!isFlagged(row, column) && (isBomb === false)){
    ++cellsMarked;
    document.getElementById(boardGame[row][column]).style.backgroundColor = "green";
    document.getElementById(boardGame[row][column]).innerHTML = cellValues[row][column];
  }
  checkIfWon();
}

function isFlagged(row, column) {
  if (document.getElementById((boardGame[row][column]).style.backgroundColor === "yellow") {
    return true;
  }
  return false;
}

function isRevealed(row, column) {
  if (document.getElementById(boardGame[row][column]).style.backgroundColor === "green") {
    return true;
  } 
  return false;
}

function flagCell(row, column) {
  if ((flagNo > 0) && (!isFlagged(row, column)) && (!isRevealed(row, column)) && (isBomb == false)) {
    document.getElementById(boardGame[row][column]).style.backgroundColor = "yellow";
    --flagNo;
    ++cellsMarked;
    checkIfWon();
    flagsRemaining();
  }
}

function calculateAdjBombs(row, column) {
  for (let a = row - 1; a <= row + 1; ++a) {
    if ((a < 1) || (a > maxRows)) {
      continue;
    } else {
      for (let b = column - 1; b <= column + 1; ++b) {
        if ((b < 1) || (b > maxColumns) || ((a === row) && (b === column))) {
          continue;
        } else {
          ++cellValues[a][b];
        }
      }
    }
  }
}

function randomBombs(number) {
  return Math.floor(Math.random() * number) + 1;
}

function placeBombs() {
  let row, column;
  for (row = 1; row <= maxRows; ++row) {
    bombs[row] = [];
  }
  for (row = 1; row <= maxRows; ++row){
    for (column = 1; b <= maxColumns; ++column) { 
      bombs[row][column] = 0;
    }
  }
  for (row = 1; row <= maxRows; ++row) {
    cellValues[row] = [];
  }
  for (row = 1; row <= maxRows; ++row){
    for (column = 1; b <= maxColumns; ++column) { 
      cellValues[row][column] = 0;
    }
  }
  for (let a = 1; a <= maxBombsNo; ++a) {
    row = randomBombs(maxRows);
    column = randomBombs(maxColumns);
    while (bombs[row][column] === 1){
      row = randomBombs(maxRows);
      column = randomBombs(maxColumns);
    }
    calculateAdjBombs(row, column);
    bombs[row][column] = 1;
  }
}       
      
